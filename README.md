# Visionutils

This is not an application in itself but is intended to be a template for writing command line computer vision applications.  It includes loading and saving PNG files and doing some image processing such as edge detection and adaptive thresholding.

I've used an ultra-permissive license so that this code could be used in any project for any purpose, possibly including closed source commercial projects.

Why not just use OpenCV?

OpenCV may be the best choice in many cases, but if you want something lightweight written in C and only need to do a few basic image transformations with minimal complexity then this could be a better option.
