
all:
	gcc -Wall -std=c99 -pedantic -O3 -o visionutil -Isrc src/*.c -lm -fopenmp

debug:
	gcc -Wall -std=c99 -pedantic -g -o visionutil -Isrc src/*.c -lm -fopenmp

clean:
	rm -f src/*.plist visionutil *.png
