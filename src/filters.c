/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  This is intended to discover a small number of image filters
 *  which can then act as basis functions. It uses a winner-takes-all
 *  type of learning.
 *
 *  Copyright (c) 2011-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/


#include "visionutils.h"

/* this assumes that the filter array contains values
   in the range 0.0 - 1.0 */
static float filter_convolve(unsigned char * img,
                             int img_width, int img_height,
                             int bitsperpixel,
                             float * filter,
                             int filter_width,
                             int tx, int ty)
{
    const float mult = 1.0f / 256.0f;
    float result = 0, diff;
    int x, y, n, s=0, c;
    int bytesperpixel = bitsperpixel/8;
    int pixels = filter_width * filter_width * bytesperpixel;

    if (bitsperpixel == 8) {
        for (y = ty; y < ty+filter_width; y++) {
            for (x = tx; x < tx+filter_width; x++, s++) {
                n = y*img_width + x;
                diff = (img[n]*mult) - filter[s];
                result += diff;
            }
        }
    }
    else {
        for (y = ty; y < ty+filter_width; y++) {
            for (x = tx; x < tx+filter_width; x++, s++) {
                n = (y*img_width + x)*bytesperpixel;
                for (c = 0; c < bytesperpixel; c++) {
                    diff = (img[n+c]*mult) - filter[s];
                    result += diff;
                }
            }
        }
    }
    return fabs(tanh(result*6/pixels))*255;
}

/* applies the given filter to the image and produces an output image.
   Filter values are assumed to be in the range 0.0 - 1.0 */
void filter_image(unsigned char * img,
                  int img_width, int img_height,
                  int bitsperpixel,
                  float * filter,
                  int filter_width,
                  unsigned char * output_img,
                  int output_img_width,
                  int output_img_height,
                  int output_bitsperpixel)
{
    int x, y, x0, y0, tx, ty, n=0, c;
    int filter_radius = filter_width/2;
    float v;
    int output_bytesperpixel = output_bitsperpixel/8;

    for (y = 0; y < output_img_height; y++) {
        y0 = y * img_height / output_img_height;
        ty = y0 - filter_radius;
        if (ty < 0) ty = 0;
        if (ty >= img_height-filter_width) continue;
        for (x = 0;
             x < output_img_width; x++, n++) {
            x0 = x * img_width / output_img_width;
            tx = x0 - filter_radius;
            if (tx < 0) tx = 0;
            if (tx >= img_width-filter_width) continue;

            v = filter_convolve(img, img_width, img_height,
                                bitsperpixel, filter, filter_width,
                                tx, ty);
            if (v < 0) v = 0;
            if (v > 255) v = 255;
            v = 255-v;

            if (output_bitsperpixel==8) {
                output_img[n] = (unsigned char)v;
            }
            else {
                for (c = 0; c < output_bytesperpixel; c++) {
                    output_img[n*output_bytesperpixel + c] =
                        (unsigned char)v;
                }
            }
        }
    }
}
