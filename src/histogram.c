/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - example computer vision functions
 *  Copyright (c) 2011-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "visionutils.h"

/* return the mean reflectance from a reflectance histogram */
int histogram_mean_reflectance(int * histogram)
{
    unsigned int i, mean_reflectance=0,total=0;

    for (i=0;i<256;i++) {
        total += histogram[i];
        mean_reflectance += i*histogram[i];
    }
    if (total==0) total=1;
    return mean_reflectance / total;
}

/* create a histogram from a region of an image */
void region_histogram(unsigned char * img,
                      int width, int height,
                      int tx, int ty, int bx, int by,
                      int bitsperpixel,
                      int * histogram)
{
    int i,x,y,n,reflectance;
    const int step = 2;
    int bytesperpixel = bitsperpixel/8;

    memset((void*)histogram,'\0',256*sizeof(int));

    if (tx < 0) tx = 0;
    if (ty < 0) ty = 0;
    if (bx >= width) bx = width-1;
    if (by >= height) by = height-1;

    for (y = ty; y <= by; y+=step) {
        for (x = tx; x <= bx; x+=step) {
            n = (y*width*bytesperpixel) + (x*bytesperpixel);
            reflectance = 0;
            for (i = 0; i < bytesperpixel; i++) {
                reflectance += img[n++];
            }
            histogram[reflectance/bytesperpixel]++;
        }
    }
}
