/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - example computer vision functions
 *  Copyright (c) 2011-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/


#include "visionutils.h"

#ifndef ABS
#define ABS(a) (((a) < 0) ? -(a) : (a))
#endif

#define SQUARE_MAG(a,b) (a*a + b*b)

/* You might need to experiment with this,
   depending upon your image size */
#define MAX_EDGES 1000000


struct Kernel {
    unsigned int Size;
    float * Data;
};


struct canny_params {
    float    GAUSSIAN_CUT_OFF;
    float    MAGNITUDE_SCALE;
    float    MAGNITUDE_LIMIT;
    float    MAGNITUDE_LIMIT_SQR;
    int      MAGNITUDE_MAX;

    float    gaussianKernelRadius;
    float    lowThreshold;
    float    highThreshold;
    unsigned int gaussianKernelWidth;

    struct Kernel   kernel;
    struct Kernel   diffKernel;
    unsigned int picSize;

    int    * data;
    int    * magnitude;

    float  * xConv;
    float  * yConv;
    float  * xGradient;
    float  * yGradient;

    int    * edge_pixel_index;
    float  * edge_magnitude;

    int     automaticThresholds;
    int    * edges;
    int      no_of_edges;
    unsigned char * edgesImage;
    int     sampling_radius_percent;
    int     image_contrast;

    float   contrast_multiplier;
    float   lowThresholdOffset;
    float   lowThresholdMultiplier;
    float   highhresholdOffset;
    float   highhresholdMultiplier;
};


int params_initialised = 0;
struct canny_params cannyparams;

void init_line_segments(struct line_segments * segments)
{
    segments->max_members = 1000000;
    segments->max_segments = 5000;
    segments->image_border = 0;
    segments->no_of_segments = 0;
    segments->ignore_periphery = 0;
    segments->members =
        (int*)malloc(segments->max_members*2*sizeof(int));
    segments->no_of_members =
        (int*)malloc(segments->max_segments*sizeof(int));
    segments->minimum_segment_length = 20;
}

void free_line_segments(struct line_segments * segments)
{
    free(segments->members);
    free(segments->no_of_members);
}

/*! \brief initialise arrays */
void canny_init_arrays(struct canny_params * params)
{
    unsigned int i;
    unsigned int picSize = params->picSize;

    params->edges = (int*)malloc(picSize*sizeof(int));
    params->edgesImage =
        (unsigned char*)malloc(picSize*sizeof(unsigned char));
    params->data = (int*)malloc(picSize*sizeof(int));
    params->magnitude = (int*)malloc(picSize*sizeof(int));
    params->xConv = (float*)malloc(picSize*sizeof(float));
    params->yConv = (float*)malloc(picSize*sizeof(float));
    params->xGradient = (float*)malloc(picSize*sizeof(float));
    params->yGradient =
        (float*)malloc(picSize*sizeof(float));
    params->edge_pixel_index =
        (int*) malloc(picSize*sizeof(int));
    params->edge_magnitude =
        (float*) malloc(picSize*sizeof(float));

    for (i = 0; i < params->picSize; i++) {
        params->data[i] = 0;
        params->magnitude[i] = 0;
        params->xConv[i] = 0;
        params->yConv[i] = 0;
        params->xGradient[i] = 0;
        params->yGradient[i] = 0;
        params->edge_pixel_index[i] = 0;
        params->edge_magnitude[i] = 0;
        params->edgesImage[i] = 255;
    }
}

void canny_init(struct canny_params * params, float edge_radius)
{
    params->GAUSSIAN_CUT_OFF = 0.005f;
    params->MAGNITUDE_SCALE = 100.0f;
    params->MAGNITUDE_LIMIT = 1000.0f;
    params->MAGNITUDE_LIMIT_SQR =
        (params->MAGNITUDE_LIMIT * params->MAGNITUDE_LIMIT);
    params->MAGNITUDE_MAX =
        ((int)(params->MAGNITUDE_SCALE * params->MAGNITUDE_LIMIT));

    params->sampling_radius_percent = edge_radius;
    params->lowThreshold = 2.5;
    params->highThreshold = 7.5;
    params->gaussianKernelRadius = 2.0;
    params->gaussianKernelWidth = 8;
    params->picSize = 0;
    params->automaticThresholds = 1;

    params->contrast_multiplier = 0.35f;
    params->lowThresholdOffset = 1.6f;
    params->lowThresholdMultiplier = 6.4f;
    params->highhresholdOffset = 2.0f;
    params->highhresholdMultiplier = 8.0f;

    params->data = NULL;
    params->magnitude = NULL;
    params->xConv = NULL;
    params->yConv = NULL;
    params->xGradient = NULL;
    params->yGradient = NULL;
    params->edge_pixel_index = NULL;
    params->edge_magnitude   = NULL;
    params->edgesImage       = NULL;

    params->kernel.Size = 0;
    params->kernel.Data = NULL;
    params->diffKernel.Size = 0;
    params->diffKernel.Data = NULL;
}

void canny_free(struct canny_params * params)
{
    if (params->kernel.Data != NULL) {
        free(params->data);
        free(params->magnitude);
        free(params->xConv);
        free(params->yConv);
        free(params->xGradient);
        free(params->yGradient);
        free(params->kernel.Data);
        free(params->diffKernel.Data);
        free(params->edge_pixel_index);
        free(params->edge_magnitude);
        free(params->edgesImage);
        free(params->edges);
    }
}

float canny_gaussian(float x, float sigma)
{
    return exp(-(x * x) / (2.0f * sigma * sigma));
}

/* non-recursive edge folowing */
void canny_follow(unsigned int x1,
                  unsigned int y1,
                  unsigned int i1,
                  int * threshold,
                  int * followedEdges,
                  int * magnitude,
                  int * width,
                  int * height)
{
    int following = 1;
    unsigned int x0,x2,y0,y2,x,y,i2,i3;

    while (following!=0) {
        following = 0;

        x0 = x1 == 0 ? x1 : x1 - 1;
        x2 = x1 == (unsigned int)*width - 1 ? x1 : x1 + 1;
        y0 = y1 == 0 ? y1 : y1 - 1;
        y2 = y1 == (unsigned int)*height - 1 ? y1 : y1 + 1;

        followedEdges[i1] = magnitude[i1];

        y = y0;
        while ((y <= y2) && (following==0)) {
            i2 = y * *width;
            i3 = i2 + x0;

            x = x0;
            while ((x <= x2) && (following==0)) {
                if (followedEdges[i3] == 0) {
                    if(magnitude[i3] >= *threshold) {
                        i1 = i3;
                        x1 = x;
                        y1 = y;
                        following = 1;
                    }
                }
                x++;
                i3++;
            }
            y++;
        }
    }
}

void canny_perform_hysteresis(int width, int height,
                              int low,
                              int high,
                              int * followedEdges,
                              int no_of_edges,
                              struct canny_params * params)
{
    int imageSize = width * height;
    int offset = imageSize - 1;
    int w,h,i,index,x,y;

    memset(followedEdges, 0, offset * sizeof(int));

    w = width;
    h = height;

    for (i = no_of_edges - 1; i >= 0; i--) {
        index = params->edge_pixel_index[i];

        if (followedEdges[index] == 0) {
            if (params->magnitude[index] >= high) {
                y = index % w;
                x = index - (y * w);
                canny_follow(x,y,index,&low,
                             followedEdges,
                             params->magnitude,&w,&h);
            }
        }
    }
}

void canny_get_thresholds(unsigned int histogram[],
                          float * meanDark,
                          float * meanLight)
{
    /*float Tmin = 0.0f;
      float Tmax = 0.0f;*/
    float minVariance = 999999.0f;
    float currMeanDark = 0.0f;
    float currMeanLight = 0.0f;
    float varianceDark = 0.0f;
    float varianceLight = 0.0f;
    float darkHits = 0.0f;
    float lightHits = 0.0f;
    /*float bestDarkHits = 0.0f;
      float bestLightHits = 0.0f;*/
    float histogramSquaredMagnitude[256] = {0};
    int h = 0;
    int bucket = 0;
    float magnitudeSqr = 0.0f;
    float variance = 0.0f;
    float divisor= 0.0f;
    unsigned int i;
    int greyLevel;
    *meanDark = 0;
    *meanLight = 0;

    /* Calculate squared magnitudes -
       avoids unneccessary multiplies later on */
    for(i = 0; i < 256; i++) {
        histogramSquaredMagnitude[i] =
            histogram[i] * histogram[i];
    }

    /* Evaluate all possible thresholds */
    for(greyLevel = 255; greyLevel >= 0; greyLevel--) {
        darkHits = 0;
        lightHits = 0;
        currMeanDark = 0;
        currMeanLight = 0;
        varianceDark = 0;
        varianceLight = 0;

        bucket = (int)greyLevel;

        for(h = 255; h >= 0; h--) {
            magnitudeSqr = histogramSquaredMagnitude[h];
            if (h < bucket) {
                currMeanDark += h * magnitudeSqr;
                varianceDark += (bucket - h) * magnitudeSqr;
                darkHits += magnitudeSqr;
            }
            else {
                currMeanLight += h * magnitudeSqr;
                varianceLight += (bucket - h) * magnitudeSqr;
                lightHits += magnitudeSqr;
            }
        }

        if (darkHits > 0) {
            /* Rescale into 0-255 range */
            divisor = darkHits * 256;
            currMeanDark = (currMeanDark * 255) / divisor;
            varianceDark = (varianceDark * 255) / divisor;
        }

        if (lightHits > 0) {
            /* Rescale into 0-255 range */
            divisor = lightHits * 256;
            currMeanLight = (currMeanLight * 255) / divisor;
            varianceLight = (varianceLight * 255) / divisor;
        }

        variance = varianceDark + varianceLight;
        if (variance < 0) {
            variance = -variance;
        }

        if (variance < minVariance) {
            minVariance = variance;
            /*Tmin = greyLevel;*/
            *meanDark = currMeanDark;
            *meanLight = currMeanLight;
            /*bestDarkHits = darkHits;
              bestLightHits = lightHits;*/
        }

        if ((int)(variance * 1000) == (int)(minVariance * 1000)) {
            /*Tmax = greyLevel;*/
            *meanLight = currMeanLight;
            /*bestLightHits = lightHits;*/
        }
    }
}

void canny_auto_threshold(unsigned char * img,
                          int width, int height,
                          int samplingStepSize,
                          struct canny_params * params)
{
    unsigned int tx =
        (unsigned int)(width * params->sampling_radius_percent / 100);
    unsigned int ty =
        (unsigned int)(height * params->sampling_radius_percent / 100);
    unsigned int bx = (unsigned int)(width - 1 - tx);
    unsigned int by = (unsigned int)(height - 1 - ty);
    unsigned int histogram[256] = {0};
    float meanDark = 0;
    float meanLight = 0;
    float contrast = 0;
    float fraction = 0;
    unsigned int n = (ty * (unsigned int)width) + tx;
    unsigned int vertical_increment =
        (unsigned int)(width * samplingStepSize);
    unsigned int x,y,n2;
    float contrast_mult;

    for (y = ty; y <= by;
         y += samplingStepSize, n += vertical_increment) {
        n2 = n;

        for (x = tx; x <= bx; x += samplingStepSize, n2++)
            histogram[img[n2]]++;
    }

    canny_get_thresholds(histogram, &meanDark, &meanLight);
    meanDark /= 255.0f;
    meanLight /= 255.0f;
    contrast = meanLight - meanDark;

    params->image_contrast = (int)(contrast*255);

    contrast_mult =
        (1.0f - (contrast * params->contrast_multiplier));
    contrast *= contrast_mult;

    fraction = (contrast - 0.048f) / (0.42f - 0.048f);

    params->lowThreshold =
        params->lowThresholdOffset +
        (fraction * params->lowThresholdMultiplier);
    params->highThreshold =
        params->highhresholdOffset +
        (fraction * params->highhresholdMultiplier);
}

void canny_create_masks(float kernelRadius,
                        unsigned int kernelWidth,
                        struct canny_params * params)
{
    int kw_length,kwidth;
    float g1,g2,g3;

    if ((params->kernel.Size == 0) ||
        (params->kernel.Size != kernelWidth)) {
        params->kernel.Data =
            (float*) realloc(params->kernel.Data,
                             kernelWidth*sizeof(float));
        params->kernel.Size = kernelWidth;
        params->diffKernel.Data =
            (float*) realloc(params->diffKernel.Data,
                             kernelWidth*sizeof(float));
        params->diffKernel.Size = kernelWidth;

        kw_length = kernelWidth * sizeof(unsigned int);
        memset(params->kernel.Data, 0, kw_length);
        memset(params->diffKernel.Data, 0, kw_length);

        for (kwidth = 0; kwidth < (int)kernelWidth; kwidth++) {
            g1 = canny_gaussian(kwidth, kernelRadius);
            if (g1 <= params->GAUSSIAN_CUT_OFF && kwidth >= 2)
                break;
            g2 = canny_gaussian(kwidth - 0.5, kernelRadius);
            g3 = canny_gaussian(kwidth + 0.5, kernelRadius);
            params->kernel.Data[kwidth] =
                ((g1 + g2 + g3) / 3.0) /
                (2.0 * 3.1415927f * kernelRadius * kernelRadius);
            params->diffKernel.Data[kwidth] = g3 - g2;
        }
    }
}

int canny_compute_gradients(int * data,
                            int width, int height,
                            float kernelRadius,
                            unsigned int kernelWidth,
                            struct canny_params * params)
{
    int initX = 0;
    int maxX = 0;
    int initY = 0;
    int maxY = 0;
    int x,y,w, no_of_edges = 0;
    float sum,sumX,sumY,k;
    unsigned int xOffset,i;
    int yOffset,index,index1,index2,index3,index4;
    float * kern;
    float xGrad,yGrad,gradMag,tmp,xGrad_abs,yGrad_abs;
    int is_edge;
    int indexNE,indexE,indexSW,indexW;
    int indexN,indexS,indexSE,indexNW;
    float neMag,sumGrad,eMag,swMag,wMag;
    float nMag,sMag,seMag,nwMag;

    canny_create_masks(kernelRadius, kernelWidth, params);

    initX = kernelWidth - 1;
    maxX = width - (kernelWidth - 1);
    initY = width * (kernelWidth - 1);
    maxY = width * (height - (kernelWidth - 1));
    w = width;

    for (y = initY; y < maxY; y+= w) {
        for(x = initX; x < maxX; x++) {
            index = x + y;
            sumX = data[index] * params->kernel.Data[0];
            sumY = sumX;
            xOffset = 1;
            yOffset = w;
            index1 = index - yOffset;
            index2 = index + yOffset;
            index3 = index - xOffset;
            index4 = index + xOffset;
            k = 0.0f;
            for (xOffset = 1; xOffset < kernelWidth;
                 xOffset++, index3--,index4++) {
                k = params->kernel.Data[xOffset];

                sumY += k * (data[index1] + data[index2]);
                sumX += k * (data[index3] + data[index4]);
                index1 -= w;
                index2 += w;
            }

            params->yConv[index] = sumY;
            params->xConv[index] = sumX;
        }
    }

    kern = params->diffKernel.Data;
    for(x = initX; x < maxX; x++) {
        for(y = initY; y < maxY; y += w) {
            sum = 0.0f;
            index = x + y;
            for (i = 1; i < kernelWidth; i++) {
                sum +=
                    kern[i] *
                    (params->yConv[index - i] -
                     params->yConv[index + i]);
            }
            params->xGradient[index] = sum;
        }
    }

    for (x = (int)kernelWidth; x < w - (int)kernelWidth; x++) {
        for (y = initY; y < maxY; y += w) {
            sum = 0.0f;
            index = x + y;
            yOffset = w;
            for (i = 1; i < kernelWidth; i++) {
                sum +=
                    kern[i] *
                    (params->xConv[index - yOffset] -
                     params->xConv[index + yOffset]);
                yOffset += w;
            }

            params->yGradient[index] = sum;
        }
    }

    initX = kernelWidth;
    maxX = width - kernelWidth;
    initY = width * kernelWidth;
    maxY = width * (height - kernelWidth);

    for (y = initY; y < maxY; y += width) {
        for (x = initX; x < maxX; x++) {
            index = x + y;

            xGrad = params->xGradient[index];
            yGrad = params->yGradient[index];

            gradMag = SQUARE_MAG(xGrad, yGrad);

            /* perform non-maximal supression */
            tmp = 0;

            xGrad_abs = ABS(xGrad);

            yGrad_abs = ABS(yGrad);

            is_edge = 0;

            if (xGrad * yGrad <= 0.0f) {
                indexNE = index - w + 1;
                neMag = SQUARE_MAG(params->xGradient[indexNE],
                                   params->yGradient[indexNE]);
                sumGrad = xGrad + yGrad;

                if (xGrad_abs >= yGrad_abs) {
                    indexE = index + 1;
                    eMag = SQUARE_MAG(params->xGradient[indexE],
                                      params->yGradient[indexE]);
                    if ((tmp = xGrad_abs * gradMag) >=
                        ABS((yGrad * neMag) - (sumGrad * eMag))) {
                        indexSW = index + w - 1;
                        indexW = index - 1;
                        swMag = SQUARE_MAG(params->xGradient[indexSW],
                                           params->yGradient[indexSW]);
                        wMag = SQUARE_MAG(params->xGradient[indexW],
                                          params->yGradient[indexW]);

                        if (tmp >
                            ABS((yGrad * swMag) - (sumGrad * wMag)))
                            is_edge = 1;
                    }
                }
                else {
                    indexN = index - w;
                    nMag = SQUARE_MAG(params->xGradient[indexN],
                                      params->yGradient[indexN]);
                    if ((tmp = ABS(yGrad * gradMag)) >=
                        ABS((xGrad * neMag) - (sumGrad * nMag))) {
                        indexS = index + w;
                        indexSW = indexS - 1;
                        swMag = SQUARE_MAG(params->xGradient[indexSW],
                                           params->yGradient[indexSW]);
                        sMag = SQUARE_MAG(params->xGradient[indexS],
                                          params->yGradient[indexS]);

                        if (tmp > ABS((xGrad * swMag) - (sumGrad * sMag))) {
                            is_edge = 1;
                        }
                    }
                }
            }
            else {
                indexSE = index + w + 1;

                seMag = SQUARE_MAG(params->xGradient[indexSE],
                                   params->yGradient[indexSE]);
                if (xGrad_abs >= yGrad_abs) {
                    indexE = index + 1;
                    eMag = SQUARE_MAG(params->xGradient[indexE],
                                      params->yGradient[indexE]);
                    if ((tmp = xGrad_abs * gradMag) >=
                        ABS((yGrad * seMag) + ((xGrad - yGrad) * eMag)))
                        {
                            indexNW = index - w - 1;
                            indexW = index - 1;
                            nwMag = SQUARE_MAG(params->xGradient[indexNW],
                                               params->yGradient[indexNW]);
                            wMag = SQUARE_MAG(params->xGradient[indexW],
                                              params->yGradient[indexW]);

                            if (tmp >
                                ABS((yGrad * nwMag) +
                                    ((xGrad - yGrad) * wMag))) {
                                is_edge = 1;
                            }
                        }
                }
                else {
                    indexS = index + w;
                    sMag = SQUARE_MAG(params->xGradient[indexS],
                                      params->yGradient[indexS]);
                    if ((tmp = yGrad_abs * gradMag) >=
                        ABS((xGrad * seMag) + ((yGrad - xGrad) * sMag))) {
                        indexN = index - w;
                        indexNW = indexN - 1;
                        nwMag = SQUARE_MAG(params->xGradient[indexNW],
                                           params->yGradient[indexNW]);
                        nMag = SQUARE_MAG(params->xGradient[indexN],
                                          params->yGradient[indexN]);
                        if (tmp >
                            ABS((xGrad * nwMag) +
                                ((yGrad - xGrad) * nMag)))
                            is_edge = 1;
                    }
                }
            }

            if (is_edge!=0) {
                if (gradMag >= params->MAGNITUDE_LIMIT_SQR)
                    params->edge_magnitude[index] = -1;
                else
                    params->edge_magnitude[index] = gradMag;

                params->edge_pixel_index[no_of_edges++] = index;

                if (gradMag < 0)
                    params->magnitude[index] = params->MAGNITUDE_MAX;
                else
                    params->magnitude[index] =
                        (int)(params->MAGNITUDE_SCALE * sqrt(gradMag));
            }
        }
    }

    return(no_of_edges);
}

void canny_threshold_edges(int width, int height,
                           int * data,
                           int * edges,
                           int * no_of_edges)
{
    int x,y,i;

    *no_of_edges = 0;
    x = width-1;
    y = height-1;
    for (i = (width * height) - 1; i >= 0; i--) {
        if (data[i] != 0) {
            data[i] = 0;
            if (*no_of_edges<MAX_EDGES) {
                edges[*no_of_edges * 2] = x;
                edges[*no_of_edges * 2 + 1] = y;
                *no_of_edges = *no_of_edges + 1;
            }
        }
        else {
            data[i] = 255;
        }
        x--;
        if (x < 0) {
            x += width;
            y--;
        }
    }
}

void canny_update(unsigned char * img,
                  int width, int height,
                  struct canny_params * params)
{
    int low = 0;
    int high = 0;
    int no_of_edges, pixels,x,y,i;

    if (params->automaticThresholds!=0) {
        canny_auto_threshold(img, width, height, 2, params);
    }

    params->picSize = width * height;


    canny_init_arrays(params);

    for (i=0;i<(int)params->picSize;i++) {
        params->data[i] = img[i];
    }

    no_of_edges =
        canny_compute_gradients(params->data,width,height,
                                params->gaussianKernelRadius,
                                params->gaussianKernelWidth,
                                params);

    low = (int)((params->lowThreshold *
                 params->MAGNITUDE_SCALE) + 0.5f);
    high = (int)((params->highThreshold *
                  params->MAGNITUDE_SCALE) + 0.5f);

    canny_perform_hysteresis(width,height,
                             low, high,
                             params->data, no_of_edges,
                             params);

    canny_threshold_edges(width,height,params->data,
                          params->edges,&(params->no_of_edges));

    pixels = width * height;

    for (i = pixels-1; i >= 0; i--) {
        params->edgesImage[i] = 255;
    }

    for (i = 0; i < params->no_of_edges*2; i += 2) {
        x = params->edges[i];
        y = params->edges[i + 1];
        params->edgesImage[(y * width) + x] = 0;
    }
}

/* The image is expected to be one byte per pixel */
void detect_edges(unsigned char * img,
                  int width, int height,
                  float threshold, float radius)
{
    int i;

    /* initialise */
    canny_init(&cannyparams, radius);

    canny_update(img,width,height,&cannyparams);

    for (i = (width*height) - 1; i >= 0; i--) {
        img[i] = cannyparams.edgesImage[i];
    }

    /* free memory */
    canny_free(&cannyparams);
}


/* traces along a set of edges and returns the
   edge members and the perimeter */
void trace_edge(unsigned char * edgesImage,
                int width, int height,
                int x, int y,
                int image_border,
                int * perimeter_tx,
                int * perimeter_ty,
                int * perimeter_bx,
                int * perimeter_by,
                int ignore_periphery,
                int max_members,
                int * members,
                int * no_of_members)
{
    int following,n,xx,yy;

    *no_of_members = 0;

    following = 1;
    while (following!=0) {
        following = 0;

        /* if we encounter the periphery of the
           image then abandon the trace */
        if ((ignore_periphery==0) ||
            ((ignore_periphery!=0) &&
             ((x >= image_border) &&
              (x <= width - 1 - image_border) &&
              (y >= image_border) &&
              (y <= height - 1 - image_border)))) {

            /* this edge has been followed */
            edgesImage[(y * width) + x] = 255;

            /* add this point to the list */
            if (*no_of_members < max_members) {
                members[*no_of_members*2] = x;
                members[*no_of_members*2+1] = y;
                *no_of_members = *no_of_members + 1;
            }

            /* update the bounding box */
            if (x < *perimeter_tx) {
                *perimeter_tx = x;
            }
            else {
                if (x > *perimeter_bx) *perimeter_bx = x;
            }

            if (y < *perimeter_ty) {
                *perimeter_ty = y;
            }
            else {
                if (y > *perimeter_by) *perimeter_by = y;
            }

            /* unrolled loops */
            yy = y - 1;
            xx = x - 1;
            if (yy > -1) {
                n = yy * width;
                if (xx > -1) {
                    if (edgesImage[n + xx]==0) {
                        x = xx;
                        y = yy;
                        following = 1;
                    }
                }
                xx = x;
                if ((following==0) &&
                    (edgesImage[n + xx]==0)) {
                    x = xx;
                    y = yy;
                    following = 1;
                }
                xx = x + 1;
                if ((following==0) && (xx < width)) {
                    if (edgesImage[n + xx]==0) {
                        x = xx;
                        y = yy;
                        following = 1;
                    }
                }
            }
            if (following == 0) {
                yy = y;
                n = yy * width;
                if (x > 0) {
                    xx = x - 1;
                    if (edgesImage[n + xx]==0) {
                        x = xx;
                        y = yy;
                        following = 1;
                    }
                }
                if ((following==0) && (x < width - 1)) {
                    xx = x + 1;
                    if (edgesImage[n + xx]==0) {
                        x = xx;
                        y = yy;
                        following = 1;
                    }
                }
            }
            if (following==0) {
                yy = y + 1;
                n = yy * width;
                if (yy < height) {
                    xx = x - 1;
                    if (xx > -1) {
                        if (edgesImage[n + xx] == 0) {
                            x = xx;
                            y = yy;
                            following = 1;
                        }
                    }
                    if (following == 0) {
                        xx++;
                        if (edgesImage[n + xx] == 0) {
                            x = xx;
                            y = yy;
                            following = 1;
                        }
                    }
                    if (following == 0) {
                        xx++;
                        if (xx < width) {
                            if (edgesImage[n + xx] == 0) {
                                x = xx;
                                y = yy;
                                following = 1;
                            }
                        }
                    }
                }
            }
        }
    }
}


/* traces along a set of edges and returns the edge members and the perimeter */
void trace_edges(unsigned char * edgesImage,
                 int width, int height,
                 struct line_segments * segments)
{
    int x,y,n,index=0,max;
    int tx = segments->image_border;
    int ty = segments->image_border;
    int bx = width - segments->image_border - 1;
    int by = height - segments->image_border - 1;
    int border_tx,border_ty,border_bx,border_by;
    int * memb,no_of_memb=0;

    segments->no_of_segments = 0;

    for (y=ty;y<=by;y++) {
        for (x=tx;x<=bx;x++) {
            n = y*width + x;
            if (edgesImage[n]==0) {
                border_tx = width;
                border_ty = height;
                border_bx = 0;
                border_by = 0;

                memb = &(segments->members[index*2]);
                max = segments->max_members - index - 1;
                trace_edge(edgesImage, width, height,
                           x, y, segments->image_border,
                           &border_tx, &border_ty,
                           &border_bx, &border_by,
                           segments->ignore_periphery,
                           max,memb,
                           &no_of_memb);
                segments->no_of_members[segments->no_of_segments] =
                    no_of_memb;

                if (segments->no_of_members[segments->no_of_segments] >
                    segments->minimum_segment_length) {
                    index +=
                        segments->no_of_members[segments->no_of_segments];
                    segments->no_of_segments++;
                    if (segments->no_of_segments >=
                        segments->max_segments) return;
                }

            }
        }
    }
}

void get_line_segments(unsigned char * edgesImage,
                       int width, int height,
                       struct line_segments * segments,
		       int min_segment_length)
{
    init_line_segments(segments);
    segments->minimum_segment_length = min_segment_length;
    trace_edges(edgesImage,width,height,segments);
}

void show_line_segments(struct line_segments * segments,
                        unsigned char * result, int width, int height,
                        int result_bitsperpixel)
{
    int i,j,index=0,x,y,n;
    unsigned char r,g,b;
    int result_bytesperpixel = result_bitsperpixel/8;

    memset(result, 0, width*height*result_bytesperpixel);

    for (i = 0; i < segments->no_of_segments; i++) {
        r = (unsigned char)(rand()%255);
        g = (unsigned char)(rand()%255);
        b = (unsigned char)(rand()%255);
        for (j = 0; j < segments->no_of_members[i]; j++,index++) {
            x = segments->members[index*2];
            y = segments->members[index*2+1];
            n = (y*width*result_bytesperpixel)+(x*result_bytesperpixel);
            result[n] = r;
            result[n+1] = g;
            result[n+2] = b;
        }
    }
}
