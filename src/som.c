/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - self-organising map
 *  Copyright (c) 2013-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "visionutils.h"

/* initialises a SOM structure */
void gpr_som_init(int dimension,
                  int no_of_sensors,
                  gpr_som * som)
{
    int i;
    gpr_som_element * elem;

    som->dimension = dimension;
    som->no_of_sensors = no_of_sensors;
    som->weight =
        (gpr_som_element**)malloc(dimension*dimension*
                                  sizeof(gpr_som_element*));
    assert(som->weight);
    for (i = 0; i < dimension*dimension; i++) {
        elem = (gpr_som_element *)&som->weight[i];

        elem->vect = (float*)malloc(no_of_sensors*sizeof(float));
    }
    som->state = (float*)malloc(dimension*dimension*sizeof(float));
    som->sensor_scale = (float*)malloc(no_of_sensors*sizeof(float));
}

/* initialise a set of image filters using the
   given image patch width */
void gpr_som_init_image_filters(int filters_dimension,
                                int patch_width,
                                gpr_som * som,
                                float max_pixel_value,
                                int patch_dimensions,
                                unsigned int * random_seed)
{
    int i;

    gpr_som_init(filters_dimension,
                 patch_width*patch_width*patch_dimensions, som);

    for (i = 0; i < patch_width*patch_width*patch_dimensions; i++) {
        gpr_som_init_sensor(som, i, 0, max_pixel_value, random_seed);
    }
}

/* returns the filter for the given map unit index */
float * gpr_som_get_filter(gpr_som * som, int index)
{
    gpr_som_element * elem;

    elem = (gpr_som_element *)&som->weight[index];
    return elem->vect;
}

/* frees a SOM structure */
void gpr_som_free(gpr_som * som)
{
    int i;
    gpr_som_element * elem;

    for (i = 0; i < som->dimension*som->dimension; i++) {
        elem = (gpr_som_element *)&som->weight[i];
        free(elem->vect);
    }
    free(som->weight);
    free(som->state);
    free(som->sensor_scale);
}

/* randomly initialise a sensor */
int gpr_som_init_sensor(gpr_som * som,
                        int sensor_index,
                        float min_value, float max_value,
                        unsigned int * random_seed)
{
    int i;
    gpr_som_element * elem;

    if (max_value <= min_value) return -1;

    som->sensor_scale[sensor_index] = 1.0f / (max_value - min_value);

    for (i = 0; i < som->dimension*som->dimension; i++) {
        elem = (gpr_som_element *)&som->weight[i];
        elem->vect[sensor_index] =
            min_value +
            ((max_value - min_value)*
             ((float)(rand_num(random_seed)%10000))/10000.0f);
    }
    return 0;
}

/* initialise all sensors within the given range of values */
void gpr_som_init_sensors(gpr_som * som,
                          float min_value, float max_value,
                          unsigned int * random_seed)
{
    int i;

    for (i = 0; i < som->no_of_sensors; i++) {
        gpr_som_init_sensor(som, i, min_value, max_value,
                            random_seed);
    }
}

/* Initialise weights for the given sensor using the given
   training data.  This ensures that there is an even
   distribution of values within the expected range */
int gpr_som_init_sensor_from_data(gpr_som * som,
                                  int sensor_index,
                                  int training_data_field_index,
                                  float * training_data,
                                  int training_data_fields_per_example,
                                  int no_of_training_examples,
                                  unsigned int * random_seed)
{
    float min_value=0, max_value=0, value;
    int i;

    /* get the minimum and maximum values for this field */
    for (i = 0; i < no_of_training_examples; i++) {
        value = training_data[i*training_data_fields_per_example +
                              training_data_field_index];
        if ((i == 0) || (value < min_value)) {
            min_value = value;
        }
        if ((i == 0) || (value > max_value)) {
            max_value = value;
        }
    }

    if (max_value <= min_value) return -1;

    /* initialise within the range of values */
    return gpr_som_init_sensor(som, sensor_index,
                               min_value, max_value,
                               random_seed);
}

/* initialise weights from an example image */
void gpr_som_init_filters_from_image(gpr_som * som,
                                     unsigned char * img,
                                     int img_width, int img_height,
                                     int bitsperpixel,
                                     int patch_dimensions,
                                     unsigned int * random_seed)
{
    int i, patch_width, tx, ty, x, y, n, s, p;
    gpr_som_element * elem;
    int bytesperpixel = bitsperpixel/8;

    if (patch_dimensions > bytesperpixel) {
        printf("Patch dimensions latger than " \
               "image bytes per pixel\n");
        return;
    }

    patch_width = (int)sqrt(som->no_of_sensors/patch_dimensions);

    /* for every filter */
    for (i = 0; i < som->dimension*som->dimension; i++) {
        elem = (gpr_som_element *)&som->weight[i];

        /* pick a random patch within the image */
        tx = (int)(rand_num(random_seed)%(img_width-patch_width));
        ty = (int)(rand_num(random_seed)%(img_height-patch_width));

        s = 0;
        for (y = ty; y < ty + patch_width; y++) {
            for (x = tx;
                 x < tx + patch_width;
                 x++, s += patch_dimensions) {
                n = ((y*img_width) + x)*bytesperpixel;
                for (p = 0; p < patch_dimensions; p++, n++) {
                  if (s+p < som->no_of_sensors) {
                    float sc = som->sensor_scale[s+p];
                    elem->vect[s+p] = (float)img[n]*sc;
                  }
                }
            }
        }
    }
}

/* returns the location of the best response */
static void gpr_som_best_response(gpr_som * som,
                                  int * x, int * y)
{
    int i;
    float max = 0;

    *x = 0;
    *y = 0;

    for (i = 0; i < som->dimension*som->dimension; i++) {
        if (som->state[i] > max) {
            max = som->state[i];
            *y = i / som->dimension;
            *x = i - ((*y)*som->dimension);
        }
    }
}

/* adjusts the weights within the SOM */
void gpr_som_learn(gpr_som * som,
                   float * sensors,
                   int inhibit_radius,
                   int excite_radius,
                   float learning_rate)
{
    int s,x=0,y=0,xx,yy,dx,dy,r;
    int inhibit_radius2;
    float diff;
    gpr_som_element * elem;

    gpr_som_best_response(som, &x, &y);

    excite_radius *= excite_radius;
    inhibit_radius2 = inhibit_radius*inhibit_radius;

    /* alter weights */
    for (xx = x - inhibit_radius; xx <= x + inhibit_radius; xx++) {
        if ((xx < 0) || (xx >= som->dimension)) continue;
        dx = xx - x;
        for (yy = y - inhibit_radius; yy <= y + inhibit_radius; yy++) {
            if ((yy < 0) || (yy >= som->dimension)) continue;
            dy = yy - y;
            elem =
                (gpr_som_element *)&som->weight[(yy*som->dimension) +
                                                xx];
            r = (dx*dx) + (dy*dy);
            if (r <= excite_radius) {
                /* excite */
                for (s = 0; s < som->no_of_sensors; s++) {
                    if ((elem->vect[s] != GPR_MISSING_VALUE) &&
                        (sensors[s] != GPR_MISSING_VALUE)) {
                        diff = sensors[s] - elem->vect[s];
                        elem->vect[s] += diff*learning_rate;
                    }
                }
            }
            else {
                if (r <= inhibit_radius2) {
                    /* inhibit */
                    for (s = 0; s < som->no_of_sensors; s++) {
                        if ((elem->vect[s] != GPR_MISSING_VALUE) &&
                            (sensors[s] != GPR_MISSING_VALUE)) {
                            diff = sensors[s] - elem->vect[s];
                            elem->vect[s] -= diff*learning_rate;
                        }
                    }
                }
            }
        }
    }
}

/* adjusts the weights within the SOM */
void gpr_som_learn_from_image(gpr_som * som,
                              unsigned char * img,
                              int img_width, int img_height,
                              int bitsperpixel,
                              int patch_dimensions,
                              int tx, int ty,
                              int inhibit_radius,
                              int excite_radius,
                              float learning_rate)
{
    int s,x=0,y=0,xx,yy,dx,dy,r,img_x,img_y,p;
    int inhibit_radius2, patch_width;
    float diff;
    gpr_som_element * elem;
    int bytesperpixel = bitsperpixel/8;

    if (learning_rate == 0) return;

    patch_width = (int)sqrt(som->no_of_sensors/patch_dimensions);

    gpr_som_best_response(som, &x, &y);

    excite_radius *= excite_radius;
    inhibit_radius2 = inhibit_radius*inhibit_radius;

    /* alter weights */
    for (xx = x - inhibit_radius; xx <= x + inhibit_radius; xx++) {
        if ((xx < 0) || (xx >= som->dimension)) continue;
        dx = xx - x;
        for (yy = y - inhibit_radius; yy <= y + inhibit_radius; yy++) {
            if ((yy < 0) || (yy >= som->dimension)) continue;
            dy = yy - y;
            elem =
                (gpr_som_element *)&som->weight[(yy*som->dimension) +
                                                xx];
            r = (dx*dx) + (dy*dy);
            if (r <= excite_radius) {
                /* excite */
                s = 0;
                for (img_y = ty;
                     img_y < ty + patch_width; img_y++) {
                    for (img_x = tx;
                         img_x < tx + patch_width;
                         img_x++, s += patch_dimensions) {
                        for (p = 0; p < patch_dimensions; p++) {
                            diff =
                                (img[(img_y*img_width + img_x)*
                                     bytesperpixel + p]*
                                 som->sensor_scale[s+p]) -
                                elem->vect[s+p];
                            elem->vect[s+p] += diff*learning_rate;
                        }
                    }
                }
            }
            else {
                if (r <= inhibit_radius2) {
                    /* inhibit */
                    s = 0;
                    for (img_y = ty;
                         img_y < ty + patch_width; img_y++) {
                        for (img_x = tx;
                             img_x < tx + patch_width;
                             img_x++, s += patch_dimensions) {
                            for (p = 0; p < patch_dimensions; p++) {
                                diff =
                                    (img[(img_y*img_width + img_x)*
                                         bytesperpixel + p]*
                                     som->sensor_scale[s+p]) -
                                    elem->vect[s+p];
                                elem->vect[s+p] -=
                                    diff*learning_rate;
                            }
                        }
                    }
                }
            }
        }
    }
}

/* updates the SOM */
int gpr_som_update(float * sensors,
                   gpr_som * som,
                   float * x, float * y)
{
    int i,winner=-1;
    float diff,min=0,max=0;
    gpr_som_element * elem;

    /* clear the state */
    memset((void*)som->state,'\0',
           som->dimension*som->dimension*sizeof(float));

    /* compare sensor values against weights.
       The sensor_scale value is used to avoid bias
       towards any particular sensor */
#pragma omp parallel for
    for (i = 0; i < som->dimension*som->dimension; i++) {
        elem = (gpr_som_element *)&som->weight[i];
        for (int s = 0; s < som->no_of_sensors; s++) {
            if ((elem->vect[s] != GPR_MISSING_VALUE) &&
                (sensors[s] != GPR_MISSING_VALUE)) {
                som->state[i] +=
                    (sensors[s] - elem->vect[s])*
                    (sensors[s] - elem->vect[s])*
                    som->sensor_scale[s];
            }
        }
    }

    /* find the winner */
    for (i = 0; i < som->dimension*som->dimension; i++) {
        diff = som->state[i];
        if ((i == 0) || (diff < min)) {
            min = diff;
            winner = i;
        }
        if ((i == 0) || (diff > max)) {
            max = diff;
        }
    }
    if (winner > -1) {
        /* return the peak location in the range 0.0 - 1.0 */
        *y = (int)(winner/som->dimension) / (float)som->dimension;
        *x = (int)(winner%som->dimension) / (float)som->dimension;
    }
    else {
        *x = -1;
        *y = -1;
    }

    /* normalise the state values in the range 0.0 - 1.0 */
    if (max > min) {
#pragma omp parallel for
        for (i = 0; i < som->dimension*som->dimension; i++) {
            som->state[i] = 1.0f - ((som->state[i] - min)/(max - min));
        }
    }
    return winner;
}

/* create a stack of soms */
static void gpr_som_stack_init_base(int no_of_layers,
                                    unsigned char *** img,
                                    int * img_width, int * img_height,
                                    int bitsperpixel,
                                    int * patch_width,
                                    int * som_dimension,
                                    gpr_som ** som,
                                    unsigned int * random_seed)
{
    int i, patch_dimensions = 1;
    int bytesperpixel = bitsperpixel/8;

    /* create an array to store the images for each layer */
    *img = (unsigned char**)malloc(no_of_layers*
                                   sizeof(unsigned char*));

    /* create an array to store the SOM for each layer */
    *som = (gpr_som*)malloc(no_of_layers*sizeof(gpr_som));

    for (i = 0; i < no_of_layers; i++) {
        /* create an image for this layer */
        if (i > 0) {
            (*img)[i] =
                (unsigned char*)malloc(img_width[i]*
                                       img_height[i]*bytesperpixel*
                                       sizeof(unsigned char));
        }

        /* initialise the SOM for this layer */
        gpr_som_init_image_filters(som_dimension[i],
                                   patch_width[i],
                                   &((*som)[i]), 255,
                                   patch_dimensions,
                                   random_seed);

        /* layers after the first one have 2 dimensional
           patches.  That is that there are two values
           for every pixel */
        patch_dimensions = 2;

        /* layers after the first one use RGB images */
        bytesperpixel = 3;
    }
}

void gpr_som_stack_init(int no_of_layers,
                        int img_width, int img_height,
                        int bitsperpixel,
                        int patch_width,
                        int * som_dimension,
                        gpr_som_stack * stack,
                        unsigned int random_seed)
{
    int i;

    stack->initialised = 0;

    stack->no_of_layers = no_of_layers;
    stack->img_width = (int*)malloc(no_of_layers*sizeof(int));
    stack->img_height = (int*)malloc(no_of_layers*sizeof(int));
    stack->patch_width = (int*)malloc(no_of_layers*sizeof(int));

    stack->inhibit_radius = 2;
    stack->excite_radius = 2;
    stack->learning_rate = 0.2f;

    stack->img_width[0] = img_width;
    stack->img_height[0] = img_height;
    stack->patch_width[0] = patch_width;
    for (i = 1; i < no_of_layers; i++) {
        stack->img_width[i] =
            stack->img_width[i-1]/stack->patch_width[i-1];
        stack->img_height[i] =
            stack->img_height[i-1]/stack->patch_width[i-1];
        stack->patch_width[i] = 2;
    }

    gpr_som_stack_init_base(stack->no_of_layers,
                            &stack->img,
                            stack->img_width,
                            stack->img_height,
                            bitsperpixel,
                            stack->patch_width,
                            som_dimension,
                            &stack->som, &random_seed);
}


/* free memory for a stack of soms */
void gpr_som_stack_free(gpr_som_stack * stack)
{
    int i;

    for (i = 1; i < stack->no_of_layers; i++) {
        free(stack->img[i]);
    }
    free(stack->som);
    free(stack->img);
    free(stack->img_width);
    free(stack->img_height);
    free(stack->patch_width);
}

void gpr_som_stack_learn(int no_of_samples,
                         unsigned char * img,
                         int img_width, int img_height,
                         int bitsperpixel,
                         gpr_som_stack * stack)
{
    int i, x, y, x0, y0, tx, ty, patch_width, patch_radius, n;
    float winner_x, winner_y;
    int patch_dimensions = 1;
    int bytesperpixel = bitsperpixel/8;

    stack->img[0] = img;
    stack->img_width[0] = img_width;
    stack->img_height[0] = img_height;

    for (i = 0; i < stack->no_of_layers; i++) {
        if (stack->initialised == 0) {
            gpr_som_init_filters_from_image(&stack->som[i],
                                            stack->img[i],
                                            stack->img_width[i],
                                            stack->img_height[i],
                                            bitsperpixel,
                                            patch_dimensions,
                                            &stack->random_seed);
        }

        if (i > 0) {
            /* width of a patch in the previous image */
            patch_width = stack->patch_width[i-1];

            /* radius of a patch in the previous image */
            patch_radius = patch_width/2;

            /* for every pixel in the image for the current layer */
#pragma omp parallel for
            for (y = 0; y < stack->img_height[i]; y++) {
                /* equivalent y coordinate in the previous image */
                y0 = y * stack->img_height[i-1] / stack->img_height[i];

                /* top coordinate of a patch in the previous image */
                ty = y0 - patch_radius;
                if (ty < 0) ty = 0;
                if (ty >= stack->img_height[i-1] - patch_width) {
                    continue;
                }
                for (x = 0; x < stack->img_width[i]; x++) {
                    n = (y*stack->img_width[i] + x)*bytesperpixel;

                    /* equivalent x coordinate in the previous image */
                    x0 = x * stack->img_width[i-1] /
                        stack->img_width[i];
                    /* top left coordinate of a patch in the
                       previous image */
                    tx = x0 - patch_radius;
                    if (tx < 0) tx = 0;
                    if (tx >= stack->img_width[i-1] - patch_width) {
                        continue;
                    }

                    /* classify the patch in the previous image */
                    gpr_som_update_from_image(stack->img[i-1],
                                              stack->img_width[i-1],
                                              stack->img_height[i-1],
                                              bitsperpixel,
                                              tx, ty,
                                              &stack->som[i-1],
                                              patch_dimensions,
                                              &winner_x, &winner_y);

                    if (winner_x < 0) {
                        continue;
                    }

                    /* the som x and y coordinates of the winner
                       is used to populate the image for the
                       current layer */
                    stack->img[i][n] = winner_x * 255;
                    stack->img[i][n+1] = winner_y * 255;
                    stack->img[i][n+2] = 0;
                }
            }
        }

        if (i == 1) {
            /* layers after the first have RGB images
               associated with them */
            bytesperpixel = 3;
        }

        /* learn filters */
        gpr_som_learn_filters_from_image(no_of_samples,
                                         stack->img[i],
                                         stack->img_width[i],
                                         stack->img_height[i],
                                         bitsperpixel, patch_dimensions,
                                         &stack->som[i],
                                         &stack->random_seed,
                                         stack->inhibit_radius,
                                         stack->excite_radius,
                                         stack->learning_rate);

        patch_dimensions = 2;
    }
    stack->initialised = 1;
}

/* learn image patch filters from the given image */
void gpr_som_learn_filters_from_image(int no_of_samples,
                                      unsigned char * img,
                                      int img_width, int img_height,
                                      int bitsperpixel,
                                      int patch_dimensions,
                                      gpr_som * som,
                                      unsigned int * random_seed,
                                      int inhibit_radius,
                                      int excite_radius,
                                      float learning_rate)
{
    int tx, ty, i;
    float x, y;
    int patch_width = (int)sqrt(som->no_of_sensors/patch_dimensions);

    for (i = 0; i < no_of_samples; i++) {
        tx = (int)(rand_num(random_seed)%(img_width-patch_width));
        ty = (int)(rand_num(random_seed)%(img_height-patch_width));

        gpr_som_update_from_image(img,
                                  img_width, img_height,
                                  bitsperpixel, tx, ty,
                                  som, patch_dimensions,
                                  &x, &y);

        gpr_som_learn_from_image(som, img,
                                 img_width, img_height,
                                 bitsperpixel,
                                 patch_dimensions,
                                 tx, ty,
                                 inhibit_radius,
                                 excite_radius,
                                 learning_rate);
    }
}

/* update the state from a patch within an image with
   top left coordinates tx,ty and return the index of the filter
   with the biggest response */
int gpr_som_update_from_image(unsigned char * img,
                              int img_width, int img_height,
                              int bitsperpixel,
                              int tx, int ty,
                              gpr_som * som,
                              int patch_dimensions,
                              float * x, float * y)
{
    int i,winner=-1,img_x,img_y,s,p;
    float diff,min=0,max=0;
    gpr_som_element * elem;
    int patch_width = (int)sqrt(som->no_of_sensors/patch_dimensions);
    int bytesperpixel = bitsperpixel/8;

    /* clear the state */
    memset((void*)som->state,'\0',
           som->dimension*som->dimension*sizeof(float));

    /* compare sensor values against weights.
       The sensor_scale value is used to avoid bias
       towards any particular sensor */
#pragma omp parallel for
    for (i = 0; i < som->dimension*som->dimension; i++) {
        elem = (gpr_som_element *)&som->weight[i];

        s = 0;
        for (img_y = ty;
             img_y < ty + patch_width; img_y++) {
            for (img_x = tx;
                 img_x < tx + patch_width;
                 img_x++, s+=patch_dimensions) {
                for (p = 0; p < patch_dimensions; p++) {
                    diff =
                        (img[(img_y*img_width + img_x)*bytesperpixel + p]*
                         som->sensor_scale[s+p]) - elem->vect[s+p];
                    som->state[i] += diff * diff;
                }
            }
        }
    }

    /* find the winner */
    for (i = 0; i < som->dimension*som->dimension; i++) {
        diff = som->state[i];
        if ((i == 0) || (diff < min)) {
            min = diff;
            winner = i;
        }
        if ((i == 0) || (diff > max)) {
            max = diff;
        }
    }
    if (winner > -1) {
        /* return the peak location in the range 0.0 - 1.0 */
        *y = (int)(winner/som->dimension) / (float)som->dimension;
        *x = (int)(winner%som->dimension) / (float)som->dimension;
    }
    else {
        *x = -1;
        *y = -1;
    }

    /* normalise the state values in the range 0.0 - 1.0 */
    if (max > min) {
#pragma omp parallel for
        for (i = 0; i < som->dimension*som->dimension; i++) {
            som->state[i] = 1.0f - ((som->state[i] - min)/(max - min));
        }
    }
    return winner;
}

/* Returns the best filter without updating the state values.
   This is less parallelisable */
int gpr_som_run_from_image(unsigned char * img,
                           int img_width, int img_height,
                           int bitsperpixel,
                           int tx, int ty,
                           gpr_som * som,
                           int patch_dimensions,
                           float * x, float * y)
{
    int i,winner=-1,img_x,img_y,s,xx,yy,p;
    float diff,score,min_score=0;
    gpr_som_element * elem;
    int patch_width = (int)sqrt(som->no_of_sensors/patch_dimensions);
    int bytesperpixel = bitsperpixel/8;

    /* compare sensor values against weights.
       The sensor_scale value is used to avoid bias
       towards any particular sensor */
    i = 0;
    for (yy = 0; yy < som->dimension; yy++) {
        for (xx = 0; xx < som->dimension; xx++, i++) {
            elem = (gpr_som_element *)&som->weight[i];

            score = 0;
            s = 0;
            for (img_y = ty;
                 img_y < ty + patch_width; img_y++) {
                for (img_x = tx;
                     img_x < tx + patch_width;
                     img_x++, s+=patch_dimensions) {
                    for (p = 0; p < patch_dimensions; p++) {
                        diff =
                            (img[(img_y*img_width + img_x)*bytesperpixel + p]*
                             som->sensor_scale[s+p]) - elem->vect[s+p];
                        score += diff * diff;
                    }
                }
            }
            if ((i == 0) || (score < min_score)) {
                min_score = score;
                winner = i;
                *x = xx;
                *y = yy;
            }
        }
    }

    return winner;
}


/* Updates the SOM without altering any state values.
   This is less parallelisable */
int gpr_som_run(float * sensors,
                gpr_som * som,
                float * x, float * y)
{
    int s,i,winner=0,xx,yy;
    float diff,min=0,ds;
    gpr_som_element * elem;

    *x=0;
    *y=0;

    /* compare sensor values against weights.
       The sensor_scale value is used to avoid bias
       towards any particular sensor */
    i = 0;
    for (yy = 0; yy < som->dimension; yy++) {
        for (xx = 0; xx < som->dimension; xx++, i++) {
            elem = (gpr_som_element *)&som->weight[i];
            diff=0;
            for (s = 0; s < som->no_of_sensors; s++) {
                if ((elem->vect[s] != GPR_MISSING_VALUE) &&
                    (sensors[s] != GPR_MISSING_VALUE)) {
                    ds = sensors[s] - elem->vect[s];
                    diff += ds * ds * som->sensor_scale[s];
                }
            }
            if ((i==0) || (diff < min)) {
                min = diff;
                winner = i;
                *x = xx;
                *y = yy;
            }
        }
    }
    return winner;
}

/* Updates an array containing outputs for the given
   training or test data. */
void gpr_som_outputs_from_data(gpr_som * som,
                               int * data_field_index,
                               float * data,
                               int fields_per_sample,
                               int no_of_samples,
                               float * result)
{
    int i,s;
    float x=0,y=0;
    float * sensors;

    sensors = (float*)malloc(som->no_of_sensors*sizeof(float));

    for (i = 0; i < no_of_samples; i++) {
        for (s = 0; s < som->no_of_sensors; s++) {
            sensors[s] = data[i*fields_per_sample +
                              data_field_index[s]];
        }
        gpr_som_run(sensors, som, &x, &y);
        result[i*2] = x;
        result[i*2 + 1] = y;
    }

    free(sensors);
}

/* learn from training data */
void gpr_som_learn_from_data(gpr_som * som,
                             int * data_field_index,
                             float * training_data,
                             int fields_per_sample,
                             int no_of_samples,
                             int learning_itterations,
                             int inhibit_radius, int excite_radius,
                             float learning_rate,
                             unsigned int * random_seed,
                             int show_progress)
{
    int i,j,n,s;
    float x=0,y=0;
    float * sensors;

    sensors = (float*)malloc(som->no_of_sensors*sizeof(float));

    for (i = 0; i < learning_itterations; i++) {
        for (j = 0; j < no_of_samples; j++) {
            /* pick a training sample */
            n = (int)(rand_num(random_seed)%no_of_samples);
            for (s = 0; s < som->no_of_sensors; s++) {
                sensors[s] = training_data[n*fields_per_sample +
                                           data_field_index[s]];
            }
            gpr_som_update(sensors, som, &x, &y);
            gpr_som_learn(som, sensors,
                          inhibit_radius, excite_radius,
                          learning_rate);
        }
        if (show_progress > 0) {
            printf(".");
            fflush(stdout);
        }
    }

    free(sensors);
}

void gpr_som_save(gpr_som * som,
                  FILE * fp)
{
    int i,s;
    int max = som->dimension*som->dimension;
    gpr_som_element * elem;

    fprintf(fp,"%d\n",som->dimension);
    fprintf(fp,"%d\n",som->no_of_sensors);
    for (i = 0; i < max; i++) {
        elem = (gpr_som_element*)&som->weight[i];
        for (s = 0; s < som->no_of_sensors; s++) {
            fprintf(fp,"%.5f\n",elem->vect[s]);
        }
    }
}

void gpr_som_load(gpr_som * som,
                  FILE * fp)
{
    char line[256];
    int i,s,ctr=0,dimension=1,no_of_sensors=1;
    gpr_som_element * elem;

    while (!feof(fp)) {
        if (fgets(line , 255 , fp) != NULL ) {
            if (strlen(line)>0) {
                switch(ctr) {
                case 0: {
                    dimension = atoi(line);
                    break;
                }
                case 1: {
                    no_of_sensors = atoi(line);
                    break;
                }
                }

                if (ctr==1) break;

                ctr++;
            }
        }
    }

    gpr_som_init(dimension, no_of_sensors, som);

    ctr=0;
    for (i = 0; i < dimension*dimension; i++) {
        elem = (gpr_som_element*)&som->weight[i];
        for (s = 0; s < no_of_sensors; s++) {
            if (fgets(line , 255 , fp) != NULL ) {
                if (strlen(line)>0) {
                    elem->vect[s] = atof(line);
                }
            }
        }
    }
}

/* show the state of the som within an image */
void gpr_som_show_state(gpr_som * som,
                        unsigned char * img,
                        int img_width, int img_height,
                        int bitsperpixel)
{
    int x, y, xx, yy, i, value, n = 0, c;
    int bytesperpixel = bitsperpixel/8;

    for (y = 0; y < img_height; y++) {
        yy = y * som->dimension / img_height;
        for (x = 0; x < img_width; x++, n += bytesperpixel) {
            xx = x * som->dimension / img_width;
            i = (yy * som->dimension) + xx;
            value = (int)(som->state[i]*255);
            for (c = 0; c < bytesperpixel; c++) {
                img[n+c] = value;
            }
        }
    }
}

/* show image patch filters within the given image */
void gpr_som_show_filters(gpr_som * som,
                          unsigned char * img,
                          int img_width, int img_height,
                          int bitsperpixel,
                          int patch_dimensions)
{
    int x, y, xx, yy, i, value, n = 0, c, s;
    int patch_width = (int)sqrt(som->no_of_sensors/patch_dimensions);
    gpr_som_element * elem;
    int bytesperpixel = bitsperpixel/8;

    if (patch_width*patch_width !=
        som->no_of_sensors/patch_dimensions) {
        printf("SOM number of sensors does not correspond " \
               "to a square patch\n");
        return;
    }

    for (y = 0; y < img_height; y++) {
        yy = y * (som->dimension * patch_width) / img_height;
        for (x = 0; x < img_width; x++, n += bytesperpixel) {
            xx = x * (som->dimension * patch_width) / img_width;

            i = ((yy/patch_width) * som->dimension) +
                (xx/patch_width);

            elem = (gpr_som_element*)&som->weight[i];

            s = (((yy%patch_width) * patch_width) +
                 (xx%patch_width))*patch_dimensions;

            value = (int)(elem->vect[s]/som->sensor_scale[s]);
            for (c = 0; c < bytesperpixel; c++) {
                img[n+c] = value;
            }
        }
    }
}
