/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - example computer vision functions
 *  Copyright (c) 2011-2018, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/


#include "visionutils.h"

/**
 * @brief deletcts vertical symmetry
 * @param img Array containing image
 * @param width Width of the image
 * @param height Height of the image
 * @param bitsperpixel Number of bits per pixel
 * @param tx Top x coordinate of the line of symmetry
 * @param ty Top y coordinate of the line of symmetry
 * @param bx Bottom x coordinate of the line of symmetry
 * @param by Bottom y coordinate of the line of symmetry
 * @return zero on success
 */
int detect_vertical_symmetry(unsigned char img[],
                             unsigned int width, unsigned int height,
                             int bitsperpixel,
                             int * tx, int * ty, int * bx, int * by)
{
    int bytes_per_pixel = bitsperpixel/8;
    int i, x, y, sample_width, offset;
    float diff, max;
    float value_left, value_right;
    float dx, dy;
    int start_x = (int)width * 3 / 10;
    int end_x = (int)width * 6 / 10;
    const int symmetry_samples = 60;
    int symmetry_sample[symmetry_samples*2];

    /* sample from a number of rows in the image */
    for (i = 0; i < symmetry_samples; i++) {
        y = ((int)height*1/10) + (i * (int)(height*8/10) / symmetry_samples);
        max = 0;
        symmetry_sample[i*2+1] = y;

        for (x = start_x; x < end_x; x++) {
            /* how wide to scan left and right */
            sample_width = x;
            if ((width - x) < sample_width)
                sample_width = width-x;

            /* sum of differences around axis of symmetry */
            diff = 0;
            for (offset = 0; offset < sample_width; offset++) {
                value_left = (float)img[(y*(int)width + x + offset)*bytes_per_pixel];
                value_right = (float)img[(y*(int)width + x - offset)*bytes_per_pixel];
                diff += fabs(value_left - value_right);
            }
            diff = 255.0f - (diff / (float)sample_width);

            /* look for peak */
            if (diff > max) {
                max = diff;
                symmetry_sample[i*2] = x;
            }
        }
    }

    *tx = 0;
    *ty = 0;
    *bx = 0;
    *by = 0;
    for (i = 0; i < symmetry_samples; i++) {
        if (i < symmetry_samples/2) {
            *tx = *tx + symmetry_sample[i*2];
            *ty = *ty + symmetry_sample[i*2+1];
        }
        else {
            *bx = *bx + symmetry_sample[i*2];
            *by = *by + symmetry_sample[i*2+1];
        }
    }
    *tx = *tx / (symmetry_samples/2);
    *ty = *ty / (symmetry_samples/2);
    *bx = *bx / (symmetry_samples/2);
    *by = *by / (symmetry_samples/2);

    /* get the bottom x coordinate of the symmetry axis */
    dx = *bx - *tx;
    dy = *by - *ty;
    *bx = *tx + ((height - *ty) * dx / dy);
    *by = height-1;

    /* get the top x coordinate of the symmetry axis */
    *tx = *tx + (-*ty * dx / dy);
    *ty = 0;

    return 0;
}
