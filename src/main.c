/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - example computer vision functions
 *  Copyright (c) 2011-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "png2.h"
#include "visionutils.h"

int main(int argc, char* argv[])
{
    int i;
    char * filename = NULL;
    char output_filename[255];
    unsigned char * image_data = NULL;
    unsigned char * hsv_image_data = NULL;
    unsigned char * hsl_image_data = NULL;
    unsigned char * meanlight_image_data = NULL;
    unsigned char * cielab_image_data = NULL;
    unsigned char * resized_image_data = NULL;
    unsigned int image_width=0;
    unsigned int image_height=0;
    unsigned int resized_image_width=0;
    unsigned int resized_image_height=0;
    unsigned int image_bitsperpixel=0;
    int adaptive_threshold_radius = 0;
    unsigned char * thresholded = NULL;
    unsigned char * filters_image_data;
    int erosion_itterations = 0;
    int dilate_itterations = 0;
    int removechannel = -1;
    int reduce_colours = 0;
    int no_of_filters=0;
    int filters_dimension;
    int filter_width=8;
    int patch_dimensions=1;
    float edge_threshold = 0;
    float edge_radius = 25;
    struct line_segments segments;
    gpr_som som;
    unsigned int random_seed = 123;
    long * integral_image;
    int difference_of_gaussians = 0;
    int som_stack_layers = 0;
    int som_stack_dimension[10];
    gpr_som_stack stack;
    unsigned char * bitwise;
    int convert_to_hsv = 0;
    int convert_to_hsl = 0;
    int convert_to_cielab = 0;
    int convert_to_meanlight = 0;
    int ml_threshold=0;
    int min_segment_length=20;
    int proximal_color[3];
    int proximal_radius=0;
    int proximal_coverage=25;

    const int max_total_polygon_points = 4096*8;
    int polygon_vertices[max_total_polygon_points];
    int polygon_id[max_total_polygon_points];
    int polygons[max_total_polygon_points*2];
    int no_of_polygons;

    memset(proximal_color, 0, 3 * sizeof(int));
    sprintf((char*)output_filename,"%s","result.png");

    for (i=1;i<argc;i+=2) {
        if ((strcmp(argv[i],"-f")==0) ||
            (strcmp(argv[i],"--filename")==0)) {
            filename = argv[i+1];
        }
        if ((strcmp(argv[i],"-r")==0) ||
            (strcmp(argv[i],"--radius")==0)) {
            adaptive_threshold_radius = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-o")==0) ||
            (strcmp(argv[i],"--output")==0)) {
            sprintf((char*)output_filename,"%s",argv[i+1]);
        }
        if ((strcmp(argv[i],"-e")==0) ||
            (strcmp(argv[i],"--edges")==0)) {
            edge_threshold = atof(argv[i+1]);
        }
        if (strcmp(argv[i],"--edgeradius")==0) {
            edge_radius = atof(argv[i+1]);
        }
        if (strcmp(argv[i],"--filters")==0) {
            no_of_filters = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-pc")==0) ||
            (strcmp(argv[i],"--coverage")==0)) {
            proximal_coverage = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-pr")==0) ||
            (strcmp(argv[i],"--proximalred")==0)) {
            proximal_color[0] = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-pg")==0) ||
            (strcmp(argv[i],"--proximalgreen")==0)) {
            proximal_color[1] = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-pb")==0) ||
            (strcmp(argv[i],"--proximalblue")==0)) {
            proximal_color[2] = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--proxradius")==0) ||
            (strcmp(argv[i],"--proximalradius")==0)) {
            proximal_radius = atoi(argv[i+1]);
        }
        if (strcmp(argv[i],"--somstack")==0) {
            som_stack_layers = atoi(argv[i+1]);
        }
        if (strcmp(argv[i],"--dog")==0) {
            difference_of_gaussians = atoi(argv[i+1]);
        }
        if (strcmp(argv[i],"--erosion")==0) {
            erosion_itterations = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--dilate")==0) ||
            (strcmp(argv[i],"--dilation")==0)) {
            dilate_itterations = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-c")==0) ||
            (strcmp(argv[i],"--removechannel")==0)) {
            removechannel = atoi(argv[i+1]);
        }
        if (strcmp(argv[i],"--enlargewidth")==0) {
            resized_image_width = atoi(argv[i+1]);
        }
        if (strcmp(argv[i],"--enlargeheight")==0) {
            resized_image_height = atoi(argv[i+1]);
        }
        if (strcmp(argv[i],"--minseglength")==0) {
            min_segment_length = atoi(argv[i+1]);
        }
        if (strcmp(argv[i],"--reduce")==0) {
            reduce_colours = 1;
            i--;
        }
        if (strcmp(argv[i],"--hsv")==0) {
            convert_to_hsv = 1;
            i--;
        }
        if (strcmp(argv[i],"--hsl")==0) {
            convert_to_hsl = 1;
            i--;
        }
        if (strcmp(argv[i],"--meanlight")==0) {
            convert_to_meanlight = 1;
            ml_threshold = atoi(argv[i+1]);
        }
        if (strcmp(argv[i],"--cielab")==0) {
            convert_to_cielab = 1;
            i--;
        }
    }

    /* was a file specified */
    if (filename == NULL) {
        printf("No image file specified\n");
        return 0;
    }

    image_data = read_png_file(filename, &image_width, &image_height, &image_bitsperpixel);
    if (image_data == NULL) {
        printf("Couldn't load image %s\n", filename);
        return 0;
    }
    if ((image_width == 0) || (image_height==0)) {
        printf("Couldn't load image size %dx%d\n", image_width, image_height);
        return 0;
    }
    if (image_bitsperpixel == 0) {
        printf("Couldn't load image depth\n");
        return 0;
    }

    printf("Image: %s\n", filename);
    printf("Resolution: %dx%d\n", image_width, image_height);
    printf("Depth: %d\n", image_bitsperpixel);

    if (reduce_colours != 0)
        if (image_bitsperpixel/8 == 3)
            rgb_colour_reduce(image_data, image_width, image_height);

    if (convert_to_hsv != 0) {
        hsv_image_data =
            (unsigned char*)malloc(image_width*image_height*
                                   image_bitsperpixel/8);
        if (hsv_image_data == NULL) {
            printf("Unable to create hsv buffer\n");
            free(image_data);
            return 1;
        }
        if (rgb_to_hsv(image_data, image_width, image_height,
                       image_bitsperpixel, hsv_image_data) != 0) {
            printf("Error creating hsv buffer\n");
            free(image_data);
            return 1;
        }
        memcpy((void*)image_data,
               (void*)hsv_image_data,
               image_width * image_height * (image_bitsperpixel/8));
        free(hsv_image_data);
    }

    if (convert_to_hsl != 0) {
        hsl_image_data =
            (unsigned char*)malloc(image_width*image_height*
                                   image_bitsperpixel/8);
        if (hsl_image_data == NULL) {
            printf("Unable to create hsl buffer\n");
            free(image_data);
            return 1;
        }
        if (rgb_to_hsl(image_data, image_width, image_height,
                       image_bitsperpixel, hsl_image_data) != 0) {
            printf("Error creating hsl buffer\n");
            free(image_data);
            return 1;
        }
        memcpy((void*)image_data,
               (void*)hsl_image_data,
               image_width * image_height * (image_bitsperpixel/8));
        free(hsl_image_data);
    }

    if (convert_to_meanlight != 0) {
        meanlight_image_data =
            (unsigned char*)malloc(image_width*image_height*sizeof(unsigned char));
        if (meanlight_image_data == NULL) {
            printf("Unable to create meanlight buffer\n");
            free(image_data);
            return 6723;
        }
        meanlight_threshold(image_data, image_width, image_height,
                            image_bitsperpixel, ml_threshold,
                            meanlight_image_data);
        free(meanlight_image_data);
    }

    if (convert_to_cielab != 0) {
        cielab_image_data =
            (unsigned char*)malloc(image_width*image_height*
                                   image_bitsperpixel/8);
        if (cielab_image_data == NULL) {
            printf("Unable to create cielab buffer\n");
            free(image_data);
            return 1;
        }
        if (rgb_to_cielab(image_data, image_width, image_height,
                          image_bitsperpixel, cielab_image_data) != 0) {
            printf("Error creating cielab buffer\n");
            free(image_data);
            return 1;
        }
        memcpy((void*)image_data,
               (void*)cielab_image_data,
               image_width * image_height * (image_bitsperpixel/8));
        free(cielab_image_data);
    }

    /* remove a particular colour channel */
    if ((removechannel > -1) && ((unsigned int)removechannel < image_bitsperpixel/8)) {
        remove_channel(image_data,image_width,image_height,image_bitsperpixel,removechannel);
    }

    if ((resized_image_width > 0) || (resized_image_height > 0)) {
        /* fill in any blanks */
        if (resized_image_width == 0) resized_image_width = image_width;
        if (resized_image_height == 0) resized_image_height = image_height;
        /* create an array to store the enlarged image */
        resized_image_data =
            (unsigned char*)malloc(resized_image_width*resized_image_height*
                                   image_bitsperpixel/8);
        if (!resized_image_data) return -653;
        /* enlarge the image */
        if (enlarge_image(image_data, image_width, image_height,
                          image_bitsperpixel,
                          resized_image_data,
                          resized_image_width, resized_image_height) != 0) {
            return -724;
        }
        /* enlarged image becomes the output */
        free(image_data);
        image_data = resized_image_data;
        image_width = resized_image_width;
        image_height = resized_image_height;
        resized_image_data = NULL;
        printf("Enlarged resolution: %dx%d\n", image_width, image_height);
    }

    if ((proximal_radius > 0) && (image_bitsperpixel == 3*8)) {
        if (thresholded == NULL) {
            thresholded = (unsigned char*)malloc(image_width*image_height);
        }
        proximal_threshold(image_data, image_width, image_height,
                           proximal_color[0], proximal_color[1], proximal_color[2],
                           proximal_radius, thresholded);

        proximal_erase(thresholded, image_width, image_height,
                       20, proximal_coverage);

        no_of_polygons = \
	  proximal_fill(thresholded, image_width, image_height,
			image_data,
			image_width*10/100,image_height*10/100,
			4,
			polygon_id,
			polygon_vertices,
			polygons,
			max_total_polygon_points);

	printf("Polygons %d\n", no_of_polygons);

	show_polygons(image_data, image_width, image_height,
		      no_of_polygons,
		      polygon_id,
		      polygon_vertices,
		      polygons);
	save_polygons_json(no_of_polygons,
			   polygon_id,
			   polygon_vertices,
			   polygons,
			   "polygons.json");
    }

    if (edge_threshold > 0) {
        if (thresholded == NULL) {
            thresholded = (unsigned char*)malloc(image_width*image_height);
        }
        colour_to_mono(image_data,
                       image_width,image_height,image_bitsperpixel,thresholded);

        printf("Detecting edges\n");
        detect_edges(thresholded,image_width,image_height,edge_threshold, edge_radius);
        printf("Done\n");

        /* convert the mono image back to colour */
        mono_to_colour(thresholded, image_width, image_height,
                       image_bitsperpixel, image_data);

        get_line_segments(thresholded, image_width, image_height, &segments, min_segment_length);
        show_line_segments(&segments, image_data, image_width, image_height, image_bitsperpixel);
        free_line_segments(&segments);
    }

    if (adaptive_threshold_radius > 0) {
        /* make a mono image with one byte per pixel */
        if (thresholded == NULL) {
            thresholded = (unsigned char*)malloc(image_width*image_height);
        }

        /* apply an adaptive threshold */
        adaptive_threshold(image_data,image_width,image_height,image_bitsperpixel,
                           adaptive_threshold_radius,thresholded);

        /* apply morphology to the mono image */
        erosion(thresholded,image_width,image_height,erosion_itterations);
        dilation(thresholded,image_width,image_height,dilate_itterations);

        if (strstr(output_filename, ".bin") != NULL) {
            /* save as bitwise image */
            if (image_width * image_height % 8 != 0) {
                free(image_data);
                free(thresholded);
                printf("Number of pixels must be divisible by 8\n");
                return 1;
            }
            bitwise = (unsigned char*)malloc(image_width*image_height/8*sizeof(unsigned char));
            mono_to_bitwise(thresholded, image_width, image_height, bitwise);
            save_bitwise(output_filename, bitwise, image_width, image_height);
            free(bitwise);
            free(thresholded);
            free(image_data);
            printf("Saved bitwise image to %s\n", output_filename);
            return 0;
        }

        /* convert the mono image back to colour */
        mono_to_colour(thresholded, image_width, image_height,
                       image_bitsperpixel, image_data);
    }
    if (thresholded != NULL) {
        /* free the memory for the mono image */
        free(thresholded);
    }

    /* save the image */
    write_png_file(output_filename, image_width, image_height, 24, image_data);

    if (difference_of_gaussians > 0) {
        integral_image = (long*)malloc(image_width*image_height*
                                       sizeof(long));

        update_centre_surround(image_data,
                               image_width, image_height, image_bitsperpixel,
                               integral_image,
                               difference_of_gaussians,
                               image_data,
                               image_width, image_height);
        free(integral_image);
    }

    if (no_of_filters > 0) {
        patch_dimensions = 1;
        filters_dimension = (int)sqrt(no_of_filters);
        gpr_som_init_image_filters(filters_dimension,
                                   filter_width, &som, 255,
                                   patch_dimensions,
                                   &random_seed);

        gpr_som_init_filters_from_image(&som,
                                        image_data,
                                        image_width, image_height,
                                        image_bitsperpixel,
                                        patch_dimensions,
                                        &random_seed);

        gpr_som_learn_filters_from_image(100000, image_data,
                                         image_width, image_height,
                                         image_bitsperpixel,
                                         patch_dimensions,
                                         &som, &random_seed,
                                         2, 2, 0.2f);

        filters_image_data =
            (unsigned char*)malloc(image_width*image_height*(image_bitsperpixel/8)*
                                   sizeof(unsigned char));
        gpr_som_show_filters(&som, filters_image_data,
                             image_width, image_height, image_bitsperpixel,
                             patch_dimensions);

        sprintf(output_filename,"%s","filters.png");
        write_png_file(output_filename, image_width, image_height, 24,
                       filters_image_data);

        /* show some example filtered images */
        for (i = 0; i < filters_dimension; i++) {
            filter_image(image_data,
                         image_width, image_height, image_bitsperpixel,
                         gpr_som_get_filter(&som, i),
                         filter_width,
                         filters_image_data,
                         image_width, image_height, image_bitsperpixel);

            sprintf(output_filename,"filtered_%03d.png",i);
            write_png_file(output_filename, image_width, image_height, 24,
                           filters_image_data);
        }

        free(filters_image_data);
        gpr_som_free(&som);
    }

    if ((som_stack_layers > 0) && (som_stack_layers < 10)) {
        /* Dimensions of the self-organising maps */
        for (i = 0; i < som_stack_layers; i++) {
            som_stack_dimension[i] = 16;
        }

        gpr_som_stack_init(som_stack_layers,
                           image_width, image_height, image_bitsperpixel,
                           filter_width, som_stack_dimension,
                           &stack, random_seed);

        gpr_som_stack_learn(10,
                            image_data,
                            image_width, image_height, image_bitsperpixel,
                            &stack);

        patch_dimensions = 1;
        for (i = 0; i < som_stack_layers; i++) {
            sprintf(output_filename,"stack_%d.png", i);
            write_png_file(output_filename,
                           stack.img_width[i],
                           stack.img_height[i],
                           24, stack.img[i]);

            gpr_som_show_filters(&stack.som[i],
                                 image_data,
                                 image_width, image_height, image_bitsperpixel,
                                 patch_dimensions);

            sprintf(output_filename,"filters_%d.png", i);
            write_png_file(output_filename, image_width, image_height,
                           24, image_data);
            patch_dimensions = 2;
        }

        gpr_som_stack_free(&stack);
    }

    /* free memory */
    free(image_data);

    printf("Ended Successfully\n");
    return 0;
}
